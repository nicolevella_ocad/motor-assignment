
/*

  Project:      Motor Assignment
  Student:      Nicole Vella
  Course:       DGIF 2002 - Physical Computing - OCAD University
  Created on:   October 8, 2019
  Based on:     Arduino Lesson 6: Digital Inputs; Simon Monk; https://learn.adafruit.com/adafruit-arduino-lesson-6-digital-inputs/
                Week 5 Lesson, Lindy Wilkins DGIF 2002 Class

*/


#include <Servo.h> // include the serve library so we can control the servo motor

// variables that will not change (like the pin# an input/output is plugged into) can be assigned as "const" to reduce overhead
const int motor1A     = 11; // motor A on digital pin 11
const int motor1B     = 12; // motor B on digital pin 12
const int servo       = 6;  // servo on digital pin 6
const int button      = 5;  // button on digital pin 5
const int photoSensor = 0;  // photosensor on analog pin 0

// variables that will change depending on input values
int angle             = 0;  // angle to turn servo to
int brightness        = 0;  // value from photosensor

Servo myservo; // initialize a servo called "myservo"


void setup() {

  // assign pins to the inputs and outputs
  pinMode(motor1A, OUTPUT);
  pinMode(motor1B, OUTPUT);
  pinMode(button, INPUT_PULLUP);

  // attach the servo to it's pin
  myservo.attach(servo);

}

void loop() {

  // basics

  if (digitalRead(button) == HIGH) {
    
    // if the button is pressed, make motor spin forward
    digitalWrite(motor1A, HIGH);
    digitalWrite(motor1B, LOW);
    
  } else {
    
    // if the button is not pressed, make motor spin backward
    digitalWrite(motor1A, LOW);
    digitalWrite(motor1B, HIGH);
    
  }



  // beyond basics
  
  brightness = analogRead(photoSensor); // get value from photoSensor, set it to brightness variable
  angle = brightness / 10; // set the angle by taking brightness value and dividing by 10 to make a number between 1 and 105
  myservo.write(angle); // set the angle on the servo

}
